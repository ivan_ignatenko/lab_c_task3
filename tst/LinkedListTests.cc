#include "gtest/gtest.h"
#include "../src/LinkedList.h"
#include "../src/LinkedList.cpp"

TEST(constructor,Norm){
    LinkedList<int> list;
    list.add(1);
    list.add(2);
    list.add(3);
    EXPECT_EQ(3,list.size());
}

TEST(find,Norm){
    LinkedList<int> list;
    list.add(1);
    list.add(2);
    list.add(3);
    auto it = list.begin();
    EXPECT_EQ(it,list.find(3));
}

TEST(Iterator,Norm){
    LinkedList<int> list;
    list.add(11);
    list.add(256);
    list.add(34);
    list.add(32);
    int arr1[4]={32,34,256,11};
    int arr2[4]={0,0,0,0};
    int i=0;
    for(int it : list){
        arr2[i] = it;
        i++;
    }
    EXPECT_EQ(arr1[0],arr2[0]);
    EXPECT_EQ(arr1[1],arr2[1]);
    EXPECT_EQ(arr1[2],arr2[2]);
    EXPECT_EQ(arr1[3],arr2[3]);
}