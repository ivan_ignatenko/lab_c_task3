//
// Created by necrobuther on 17.03.2020.
//

#ifndef TASK3_1_LINKEDLIST_H
#define TASK3_1_LINKEDLIST_H

#include <algorithm>
#include <ostream>
#include "List.h"

template <typename T>
class LinkedList: public List<T>{

public:
    class ListIterator : public Iterator<T> {
    public:
        Node<T> * getCurrent() override ;
        void setCurrent(Node<T>* node) override ;
        ListIterator();
        T operator*() override;

        const T &operator*() const override;

        ListIterator &operator++() override;

        ListIterator& operator++(int i) override;

        ListIterator&operator--() override;

        ListIterator& operator--(int i) override;

        bool operator==(const ListIterator &rhs) const;

        bool operator!=(const ListIterator &rhs) const;

    protected:
        Node<T> *current;
        T & retrieve() const;
        explicit ListIterator(Node<T> *p);
        friend class LinkedList<T>;
    };

    LinkedList();
    LinkedList(const LinkedList &rhs);

    LinkedList(LinkedList && rhs) noexcept ;

    LinkedList(ListIterator start,ListIterator  end);

    explicit LinkedList(int num, const T& val = T{});


    ~LinkedList();

    // copy
    LinkedList& operator=(const LinkedList &rhs);
    // move
    LinkedList & operator=(LinkedList && rhs);

    void add(const T e,Iterator<T>& it) override;

    T pop();

    T delete_(Iterator<T> &it) override;

    ListIterator find(const T e);

    void clear() override;

    bool empty()const override;

    unsigned int size() override;
    void add(T e) override ;

    ListIterator begin();
    ListIterator end();

    template <typename Friend_type>
    friend bool operator==(const  LinkedList<Friend_type> & lhs, const  LinkedList<Friend_type> &rhs);
    template <typename Friend_type>
    friend bool operator!=(const  LinkedList<Friend_type> & lhs, const  LinkedList<Friend_type> &rhs);
    template <typename Friend_type>
    friend std::ostream & operator<<(std::ostream &os, const  LinkedList<Friend_type> &l);

private:
    void init();
    Node<T> *head, *tail;
    unsigned _size;
};




#endif //TASK3_1_LINKEDLIST_H
