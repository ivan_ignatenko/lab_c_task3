//
// Created by necrobuther on 17.03.2020.
//

#include <tuple>
#include "LinkedList.h"
template<typename T>
LinkedList<T>::ListIterator::ListIterator(): current{nullptr}  {}

template<typename T>
T &LinkedList<T>::ListIterator::retrieve() const {
    return current->data;
}


template<typename T>
T LinkedList<T>::ListIterator::operator*() {
   return this->current->data;
}

template<typename T>
const T &LinkedList<T>::ListIterator::operator*() const {
    return retrieve();
}

template<typename T>
typename LinkedList<T>::ListIterator &LinkedList<T>::ListIterator::operator++() {
    this->current = this->current->next;
    return *this;
}



template<typename T>
typename LinkedList<T>::ListIterator &LinkedList<T>::ListIterator::operator--() {
    this->current = this->current->prev;
    return *this;
}

template<typename T>
 typename LinkedList<T>::ListIterator& LinkedList<T>::ListIterator::operator--(int i) {
    auto cpy = *this;
    this->current = this->current->prev;
    return *this;
}



template<typename T>
void LinkedList<T>::ListIterator::setCurrent(Node<T> *node) {
    this->current = node;
}

template<typename T>
LinkedList<T>::ListIterator::ListIterator(Node<T> *p):current{p} {}

template<typename T>
Node<T> *LinkedList<T>::ListIterator::getCurrent() {
    return current;
}

template<typename T>
typename LinkedList<T>::ListIterator& LinkedList<T>::ListIterator::operator++(int i){
    auto cpy = *this;
    this->current = this->current->next;
    return cpy;
}

template<typename T>
bool LinkedList<T>::ListIterator::operator==(const LinkedList::ListIterator &rhs) const {
    return /*((typeid(rhs) == typeid(this) && (this->current->next == rhs.current->next)
    && (this->current->prev == rhs.current->prev)))
    && */
    (static_cast<const Iterator<T> &>(*this), current) ==
           (static_cast<const Iterator<T> &>(rhs), rhs.current);
}

template<typename T>
bool LinkedList<T>::ListIterator::operator!=(const LinkedList::ListIterator &rhs) const {
    return !(rhs == *this);
}


template<typename T>
LinkedList<T>::LinkedList() {
    init();
}

template<typename T>
LinkedList<T>::~LinkedList() {
    clear();
    delete head;
    delete tail;
}

template<typename T>
LinkedList<T>::LinkedList(const LinkedList &rhs) {
    init();
    for(auto itr = rhs.begin(); itr != rhs.end(); ++itr)
        add(*itr);
}

template<typename T>
void LinkedList<T>::init() {
    _size = 0;
    head = new Node<T>;
    tail = new Node<T>;
    head->next = tail;
    tail->prev = head;
}

template<typename T>
LinkedList<T>::LinkedList(LinkedList &&rhs) noexcept: _size(rhs._size), head{rhs.head}, tail{rhs.tail}
{
    rhs._size = 0;
    rhs.head = nullptr;
    rhs.tail = nullptr;
}

template<typename T>
LinkedList<T> &LinkedList<T>::operator=(const LinkedList &rhs) {
    auto cpy = rhs;
    std::swap(*this, cpy);
    return *this;
}

template<typename T>
LinkedList<T> &LinkedList<T>::operator=(LinkedList &&rhs) {
    init();
    *this = std::move( rhs );
    return *this;
}

template<typename T>
LinkedList<T>::LinkedList(LinkedList::ListIterator start, LinkedList::ListIterator end) {
    init();
    for(auto itr= start; itr != end; ++itr)
        add( *itr );

}

template<typename T>
LinkedList<T>::LinkedList(int num, const T &val) {
    init();
    int index;
    for(index = 0; index < num; index++)
        add(val);
}

template<typename T>
void LinkedList<T>::add(const T e,Iterator<T>& it) {
    auto* p = it.getCurrent();
    _size++;
    auto* nptr = new Node{e, p->prev, p};
    p->prev->next = nptr;
    p->prev = nptr;
}

template<typename T>
T LinkedList<T>::delete_(Iterator<T> &it) {
    auto* p = it.getCurrent();
    ListIterator value{p->next};
    p->prev->next = p->next;
    p->next->prev = p->prev;
    delete p;
    _size--;
    return *value;
}

template<typename T>
typename LinkedList<T>::ListIterator LinkedList<T>::begin() {
    if(!empty())
    {
        ListIterator iterator{head->next};
        return iterator;
    }
}

template<typename T>
typename LinkedList<T>::ListIterator LinkedList<T>::end() {
    ListIterator itr{ tail };
    return itr;
}



template<typename T>
void LinkedList<T>::clear() {
    while(!empty()){
        pop();
    }
}

template<typename T>
T LinkedList<T>::pop() {
    delete_(--end());
}

template<typename T>
bool LinkedList<T>::empty() const {
    return _size == 0;
}

template<typename T>
unsigned int LinkedList<T>::size() {
    return _size;
}

template<typename T>
void LinkedList<T>::add(T e) {
    ListIterator it{head->next};
    auto* p = it.getCurrent();
    _size++;
    auto* nptr = new Node{e, p->prev, p};
    p->prev->next = nptr;
    p->prev = nptr;
}

template<typename Friend_type>
bool operator==(const LinkedList<Friend_type> &lhs, const LinkedList<Friend_type> &rhs) {
    bool flag = true;
    if( lhs.size() == rhs.size())
    {
        auto rhs_itr = rhs.begin();
        for(auto lhs_itr=lhs.begin(); lhs_itr !=lhs.end(); ++lhs_itr)
        {
            if(*lhs_itr != *rhs_itr)
                flag = false;
            break;
            ++rhs_itr;
        }
        return flag;
    }
    else
        return false;
}

template<typename Friend_type>
bool operator!=(const LinkedList<Friend_type> &lhs, const LinkedList<Friend_type> &rhs) {
    return !(lhs == rhs);
}

template<typename Friend_type>
std::ostream &operator<<(std::ostream &os, const LinkedList<Friend_type> &l) {
    for(auto itr = l.begin(); itr != l.end(); ++itr)
        os << *itr << os;
    return os;
}

template<typename T>
typename LinkedList<T>::ListIterator LinkedList<T>::find(const T e) {
    for(auto itr = begin(); itr != end(); ++itr)
    {
        if(*itr == e)return itr;
    }
}


