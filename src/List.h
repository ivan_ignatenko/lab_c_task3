//
// Created by necrobuther on 17.03.2020.
//

#ifndef TASK3_1_LIST_H
#define TASK3_1_LIST_H

#include "Iterator.h"

template <typename T>
class List{
public:
    virtual void add(T e,Iterator<T>& it) =0;
    virtual void add(T e) =0;
    virtual T delete_(Iterator<T>& it)=0;
    virtual void clear()=0;
    virtual bool empty() const=0;
    virtual unsigned size()=0;
};


#endif //TASK3_1_LIST_H
