//
// Created by necrobuther on 17.03.2020.
//

#ifndef TASK3_1_ITERATOR_H
#define TASK3_1_ITERATOR_H
template <typename T>
struct Node {
    T data;
    Node *prev;
    Node *next;

    explicit Node(const T &d = T{}, Node *p = nullptr, Node *n = nullptr)
            : data{d}, prev{p}, next{n} {}

    explicit Node(T &&d, Node *p = nullptr, Node *n = nullptr)
            : data{std::move(d)}, prev{p}, next{n} {}
};
template <typename T>

class Iterator{
public:
    virtual Node<T>* getCurrent()=0;
    virtual void setCurrent(Node<T>* node)=0;
    virtual T operator*()=0;
    virtual const T & operator*() const=0;
    virtual   Iterator & operator++()=0;
    virtual Iterator& operator++(int)=0;
    virtual Iterator & operator--()=0;
    virtual Iterator& operator--(int)=0;


};
#endif //TASK3_1_ITERATOR_H
